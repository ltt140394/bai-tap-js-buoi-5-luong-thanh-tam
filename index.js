function getMyEle(id) {
    return document.getElementById(id);
}

// Bài tập 1
function xetKetQuaThi() {
    var diemChuan = getMyEle("diem_chuan").value * 1;
    var diemMon1 = getMyEle("diem_mon1").value * 1;
    var diemMon2 = getMyEle("diem_mon2").value * 1;
    var diemMon3 = getMyEle("diem_mon3").value * 1;
    var khuVuc = getMyEle("khu_vuc").value;
    var doiTuong = getMyEle("doi_tuong").value;

    var diemUuTienKhuVuc, diemUuTienDoiTuong;
    if (khuVuc == "A") {
        diemUuTienKhuVuc = 2;
    } else if (khuVuc == "B") {
        diemUuTienKhuVuc = 1;
    } else if (khuVuc == "C") {
        diemUuTienKhuVuc = 0.5;
    } else {
        diemUuTienKhuVuc = 0;
    }

    if (doiTuong == 1) {
        diemUuTienDoiTuong = 2.5;
    } else if (doiTuong == 2) {
        diemUuTienDoiTuong = 1.5;
    } else if (doiTuong == 3) {
        diemUuTienDoiTuong = 1;
    } else {
        diemUuTienDoiTuong = 0;
    }

    var diemTongKet = diemMon1 + diemMon2 + diemMon3 + diemUuTienKhuVuc + diemUuTienDoiTuong;

    if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
        getMyEle("show-ket-qua-bt1").style.display = "block";
        getMyEle("show-ket-qua-bt1").innerHTML = `Rất tiếc bạn đã thi trượt do có 1 môn thi bất kì 0 điểm`
    } else if (diemTongKet < diemChuan) {
        getMyEle("show-ket-qua-bt1").style.display = "block";
        getMyEle("show-ket-qua-bt1").innerHTML = `
       <p>Tổng điểm thi : ${diemTongKet}</p>
       <p>Rất tiếc bạn đã thi trượt do không đủ điểm chuẩn</p>
       `
    } else {
        getMyEle("show-ket-qua-bt1").style.display = "block";
        getMyEle("show-ket-qua-bt1").innerHTML = `
       <p>Tổng điểm thi : ${diemTongKet}</p>
       <p>Xin chúc mừng bạn đã thi đậu</p>
       `
    }
}

// Bài tập 2
function tinhTienDien() {
    var hoTenKH = getMyEle("ho_ten").value;
    var soKwTieuThu = getMyEle("so_kw").value * 1;

    var giaTien50KwDau = 500;
    var giaTien50KwKeTiep = 650;
    var giaTien100KwKeTiep = 850;
    var giaTien150KwKeTiep = 1100;
    var giaTienConLaiTiepTheo = 1300;
    
    var tienPhaiTra;
    if(soKwTieuThu <= 50) {
        tienPhaiTra = giaTien50KwDau * soKwTieuThu;
    } else if(soKwTieuThu <= 100) {
        tienPhaiTra = giaTien50KwDau * 50 + giaTien50KwKeTiep * (soKwTieuThu - 50);
    } else if(soKwTieuThu <= 200) {
        tienPhaiTra = giaTien50KwDau * 50 + giaTien50KwKeTiep * 50 + giaTien100KwKeTiep * (soKwTieuThu - 100);
    } else if(soKwTieuThu <= 350) {
        tienPhaiTra = giaTien50KwDau * 50 + giaTien50KwKeTiep * 50 + giaTien100KwKeTiep * 100 + giaTien150KwKeTiep * (soKwTieuThu - 200);
    } else {
        tienPhaiTra = giaTien50KwDau * 50 + giaTien50KwKeTiep * 50 + giaTien100KwKeTiep * 100 + giaTien150KwKeTiep * 150 + giaTienConLaiTiepTheo * (soKwTieuThu - 350);
    }

    var tienPhaiTraFormatter = new Intl.NumberFormat('vn-VN').format(tienPhaiTra);

    getMyEle("show-ket-qua-bt2").style.display = "block";
    getMyEle("show-ket-qua-bt2").innerHTML = `
    <p>Họ và tên khách hàng: ${hoTenKH}</p>
    <p>Số tiền điện phải thanh toán: ${tienPhaiTraFormatter} VNĐ</p>
    `
}